provider "aws" {
  version = "~> 2.0"
  region  = var.aws_region
}

provider "cloudflare" {
  version = "~> 2.0"
  email   = var.cloudflare_email
  api_key = var.cloudflare_api_key
}

variable "aws_region" {
  type = string
  default = "us-east-1"
}

variable "site_domain" {
  type = string
}

variable "site_staging_subdomain" {
  type = string
  default = "staging"
}

variable "site_staging_domain" {
  type = string
}

variable "cloudflare_email" {
  type = string
}

variable "cloudflare_api_key" {
  type = string
}

module "statically_yours_akeratos" {
  source = "git::https://gitlab.com/bitemyapp/statically-yours.git?ref=ecbead3dbc0e651d3bdce7e4be0d73eb7d8c1a9a"

  aws_region = "us-east-1"
  site_domain = var.site_domain
  site_staging_subdomain = var.site_staging_subdomain
  site_staging_domain = var.site_staging_domain
  cloudflare_email = var.cloudflare_email
  cloudflare_api_key = var.cloudflare_api_key
}
