#!/usr/bin/env bash

set -ex

MODULE_NAME=module.statically_yours_akeratos
# cloudflare stuff, fill in the pre-existing stuff
ZONE_ID=
RECORD_ID=
STAGING_RECORD_ID=
WWW_RECORD_ID=

## Cloudflare resources

terraform import $MODULE_NAME.cloudflare_zone.site_zone $ZONE_ID
# terraform import $MODULE_NAME.cloudflare_zone_settings_override.site_zone_settings_override $ZONE_ID # Doesn't support import

terraform import $MODULE_NAME.cloudflare_record.site_cname $ZONE_ID/$RECORD_ID
terraform import $MODULE_NAME.cloudflare_record.site_cname_staging $ZONE_ID/$STAGING_RECORD_ID
terraform import $MODULE_NAME.cloudflare_zone.site_cname_www $WWW_RECORD_ID

## S3 resources --- you don't really need to import these. You can just apply over them if they already exist.
