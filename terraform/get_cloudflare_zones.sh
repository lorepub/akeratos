#!/usr/bin/env bash

curl -s -X GET "https://api.Cloudflare.com/client/v4/zones/?per_page=100" -H "X-Auth-Email: $CLOUDFLARE_AUTH_EMAIL" -H "X-Auth-Key: $CLOUDFLARE_AUTH_KEY" -H "Content-Type: application/json"| jq -r '.result[] | "\(.id) \(.name)"'
