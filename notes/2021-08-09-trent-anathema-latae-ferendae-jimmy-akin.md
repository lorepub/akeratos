# 2021-08-09 Council of Trent anathemas, latae or ferendae? Jimmy Akin / @paleomexicano

Some context for the others in this read, the work by Osborne you're mentioning citing Lennerz is described by the author as radically rethinking sacramental life and theology from the standpoint of postmodern philosophy.

Here's a review of Osborne's book that cited Lennerz from an OCDS:

"I have read a number of titles by Fr Osborne and I cannot understand why he did not leave the church."

Do you have a more authoritative source for this?

Interesting wording here: https://en.wikipedia.org/wiki/List_of_excommunicable_offences_from_the_Council_of_Trent

>Heresies about the Sacraments or de fide doctrines which had been rejected or re-defined by the Protestants were specified and assigned automatic excommunication for Catholics who held them

>for Catholics who held them

One thing to keep in mind is that a Protestant is excommunicated by definition regardless of whether they knowingly believe anything anathematized by Trent or not. Given that, I'm not sure what's being litigated here.

## Convo dump

Ify NsohaLatin cross
@IfyNsoha
·
8h
Protestants: if you went to heaven and found out Roman Catholicism is true, how would you react?
Vicente Fox Mulder
@paleomexicano
·
7h
I'd rejoice in God's mercy regardless
Vicente Fox Mulder
@paleomexicano
·
7h
That does presume which version of Rome: Tridentine "all prots in the fire, stack the bodies, make it higher" version or Vatican II "separated brethren hey its all good" version
“Dr.” Murray Fullerton Smiling face with haloFolded handsJapanese ogre
@MurrayFullerton
·
7h
This is so silly.
Vicente Fox Mulder
@paleomexicano
·
7h
Look I get that by definition a RC must accept that no councils contradict each other but the idea that Prots as a category go to hell was not something I made up by any stretch as some calumny
“Dr.” Murray Fullerton Smiling face with haloFolded handsJapanese ogre
@MurrayFullerton
·
5h
Can you point to where in Trent it says Protestants go to hell?
Vicente Fox Mulder
@paleomexicano
·
5h
The anathemas would, with any reasonable interpretation, place us outside the Catholic Church and thus, due their reading of extra ecclesiam nulla salus, going to hell. That's not just my reading, that interpretation is according to Fr Jos. Arsenault, SSA
“Dr.” Murray Fullerton Smiling face with haloFolded handsJapanese ogre
@MurrayFullerton
·
5h
You are incorrect. No baptised person is “outside the Church”. You also don’t seem to know who anathema is applied to. I’d suggest starting here: https://jimmyakin.com/2011/06/are-you-anathema-how-about-your-protestant-friend.html 
@JimmyAkin3000
Vicente Fox Mulder
@paleomexicano
·
5h
I've listened to Akin answer this question on CA, where he says it is a form of excommunication from the Church. One would be forced to say that those at Trent or at that time simply misunderstood the meaning of their own statements on this
“Dr.” Murray Fullerton Smiling face with haloFolded handsJapanese ogre
@MurrayFullerton
·
5h
I think you should engage with what he wrote, rather than saying what the writers of Trent believed in their minds words meant.
Vicente Fox Mulder
@paleomexicano
·
5h
I'm engaging with what he himself said, & what Trent says. If we are under anathema, if anathema is exclusion from the society of the faithful, separated from the society of all Christians, excluded from the bosom of the the Church in Heaven & on earth, then that's pretty obvious
“Dr.” Murray Fullerton Smiling face with haloFolded handsJapanese ogre
@MurrayFullerton
·
5h
You aren’t under anathema. You’d know that if you read his article. It’s not even debatable.
Vicente Fox Mulder
@paleomexicano
·
5h
I didn't say I was then. I made a distinction between post V2 and Trent (though it's also debatable whether someone who takes the time to discern conversion and declines to remains "invincibly ignorant" or not)
alejandro Performing arts
@0ratefratres
·
3h
Trent dealt w/ baptized Catholics who separated themselves from the Church. Thus, anathemas are ecclesiastical penalties. V2, OTOH, deals w/ the people born into these separated communities centuries later.
Jimmy Akin
@JimmyAkin3000
·
3h
Anathemas/excommunications don't change the state of a person's soul. E.g., when St. Athanasius was excommunicated. An unjust excommunication (like his) doesn't rip grace out of a person's soul. If the excommunication was just, the person excluded the grace himself.
Vicente Fox Mulder
@paleomexicano
·
3h
Not sure how that works with the formula proclaiming quite a lot insofar as the connection to the Catholic Church outside of which there is no salvation. Where does Trent say "Only valid if you by name have gone through a trial?" or "We are not using Pope Zachary's formula"
Jimmy Akin
@JimmyAkin3000
·
3h
Being juridically outside the Church was not understood in Trent's day to imply automatic damnation. Trent acknowledged salvation of catechumens (who were juridically outside the Church) and would have acknowledged salvation of those unjustly excommunicated, like St. Athanasius.
Vicente Fox Mulder
@paleomexicano
·
2h
So my question then would be, which form of excommunication is meant by this, Latae sententiae or a lesser form because it seems clear from the historical evidence that those at the time believed it to be a judgement on those Protestants living at that time automatically
Jimmy Akin
@JimmyAkin3000
·
2h
This is not the case. Anathema was the highest form of excommunication and did not take place latae sententiae. As a result, it was never applied to Protestants in general, only to certain people who tried to remain Catholic while teaching condemned doctrines.
Vicente Fox Mulder
@paleomexicano
·
2h
Which form of excommunication did Trent reckon if not against those who hold certain doctrines specifically why the general language of "If anyone saith...."?
Jimmy Akin
@JimmyAkin3000
·
2h
Trent says, "If anyone says X, let him be anathema." This was a ferendae sententiae penalty that required action by an ecclesiastical court to take effect. See the intro to the rite I recently posted.

Vicente Fox Mulder
@paleomexicano
Replying to 
@JimmyAkin3000
 
@0ratefratres
 and 2 others
Do you have a source for where in Trent this says so?
7:35 PM · Aug 9, 2021·Twitter Web App


Jimmy Akin
@JimmyAkin3000
·
2h
Replying to 
@paleomexicano
 
@0ratefratres
 and 2 others
For purposes of comparison, where does Genesis define the word "God" (Hebrew, El)? Genesis expects its readers to already know the meaning of this term, based on its common use in Hebrew when Genesis was written.
Vicente Fox Mulder
@paleomexicano
·
2h
Which makes the reception of said excommunications all the more relevant. Even RC writers have admitted that the reception of Trent among the RC was not some relatively minor and elongated judicial process but rather an ipso facto separation from the Body


Jimmy Akin
@JimmyAkin3000
Replying to 
@paleomexicano
 
@0ratefratres
 and 2 others
I don't know which writers you are speaking of or what they may have meant. The statement attributed to them is ambiguous.
7:44 PM · Aug 9, 2021·Twitter Web App
1
 Like


Vicente Fox Mulder
@paleomexicano
·
2h
Replying to 
@JimmyAkin3000
 
@0ratefratres
 and 2 others
Fr Johann Baptiste Umberg & Heinreich Lennerz were Roman Catholic scholars (Umberg being specific to Trent), cited by folks like Kenan Osborne, OFM
Jimmy Akin
@JimmyAkin3000
·
1h
What work of his are you citing?


Vicente Fox Mulder
@paleomexicano
Replying to 
@JimmyAkin3000
 
@0ratefratres
 and 2 others
His work on the sacraments specifically, his chapter on postmodern sacramentality specifically. There he cites the German scholars I'm referring to
8:13 PM · Aug 9, 2021·Twitter Web App


Jimmy Akin
@JimmyAkin3000
·
1h
Replying to 
@paleomexicano
 
@0ratefratres
 and 2 others
Do you have a page number or a page image with an exact quotation?
Vicente Fox Mulder
@paleomexicano
·
1h
p. 167-170 if memory serves
Vicente Fox Mulder
@paleomexicano
·
39m
Doing a deep dive here while I’m waiting for chicken to grill but the New Advent encyclopedia seems to agree with the Lennerz view that the Tridentine anathemas are latae sententiae (specifically that the modern culling of what counts as l/s “retains” the anathemas)
Vicente Fox Mulder
@paleomexicano
·
39m
It cited Ferraris’ canon law so I’m waiting for that to load and translate on his list of latae sententiae excommunications prior to this papal shuffle
