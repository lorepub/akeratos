FROM buildpack-deps:jammy-curl

MAINTAINER Chris Allen <cma@bitemyapp.com>

ENV VERSION v0.15.3

RUN apt update -y && apt install -y wget tar python3 python3-dev python3-pip && \
    pip3 install awscli 

RUN wget https://github.com/getzola/zola/releases/download/${VERSION}/zola-${VERSION}-x86_64-unknown-linux-gnu.tar.gz -O zola.tar.gz && \
    mkdir -p /opt/zola-${VERSION} && \
    tar -xzf zola.tar.gz -C /opt/zola-${VERSION}/ && \
    rm zola.tar.gz

ENV PATH $PATH:/opt/zola-${VERSION}
RUN cp /opt/zola-${VERSION}/zola /usr/bin/
