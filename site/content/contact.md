+++
title = "contact"

[extra]
notitle = true
+++

# The best ways to get in touch in with me (Chris Allen)

* [Github](http://github.com/bitemyapp/)

* [Twitter](http://twitter.com/bitemyapp/)

* Email: cma at bitemyapp dot com

* Telegram: you can find me on telegram as @bitemyapp
