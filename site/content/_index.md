+++
title = "home"
paginate_by = 5
+++

Theological resources for Catholics.

# Eastern

[Eastern resources](/eastern/)

# Masses

[Finding Mass near you](/masses)

# Topics

- [Scripture](/topics/scripture)

- [Sacraments of the Church](/topics/sacraments)

- [The Mass, what it's for, and what it means](/topics/mass)

- [Fasting](/topics/fasting)

# Blessed Virgin Mary

- [Ever Virgin](/mary/ever-virgin)

# Catechisms

- [The catechisms of the Catholic Church](/topics/catechisms)

# Scripture, letters, encyclicals, and other documents

- [Septuagint](/documents/protoevangelium-of-james)

- [Protoevangelium of James](/documents/protoevangelium-of-james)

# People

- [Philo](/people/philo)

# Notes on opinions and arguments from others

- [Eastern Orthodox apologetics](/apantiseis/orthodox)
