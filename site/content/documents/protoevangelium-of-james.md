+++
title = "The Protoevangelium of James"
+++

Not the epistle of James, probably written around 145 AD.

- [Wikipedia](https://en.wikipedia.org/wiki/Gospel_of_James)

- [Did Jewish Temple Virgins Exist and was Mary a Temple Virgin?](https://taylormarshall.com/2011/12/did-jewish-temple-virgins-exist-and-was.html)

## Translation

- [From New Advent](http://www.newadvent.org/fathers/0847.htm)
