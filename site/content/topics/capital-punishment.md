+++
title = "Capital Punishment"
+++

# Thomistic Institute

Remember that these are more academic and speculative investigations of this topic. You aren't obligated to believe a particular framing of this issue as a Catholic.

https://soundcloud.com/thomisticinstitute/dr-edward-feser-the-natural-law-justification-of-capital-punishment-keating-responding
