+++
title = "Eucharist"
+++

- [Bishop Barron on the Sacrament of the Eucharist as Sacrifice](https://www.youtube.com/watch?v=z6ROgZfVu6o)

- [Bishop Barron on the Sacrament of the Eucharist as Meal](https://www.youtube.com/watch?v=PFsFv-TWVfY)

- [Bishop Barron on the Mystery of Eating Jesus' Flesh](https://www.youtube.com/watch?v=NDVLBUK5FGw)

- [Bishop Barron on the Real Presence of Christ in the Eucharist](https://www.youtube.com/watch?v=bJjW3LXuHzo)

- [Bishop Barron on the Sacrament of the Eucharist as Real Presence](https://www.youtube.com/watch?v=sgy_TFeIyiM)

A priest can only give you the blessed sacarament of the eucharist, he cannot give you communion.

# What communion means

