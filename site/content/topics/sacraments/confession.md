+++
title = "Confession"
+++

> “I can go right to God.”  Jesus gave to the Apostles the ministry of Reconciliation. He told them to forgive sins. St James says confess your sins to one another. The priests forgives in persona Christi.
