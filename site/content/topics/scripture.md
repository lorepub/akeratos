+++
title = "Scripture"
+++

## Doctrinal topics

- [Sola scriptura](/topics/scripture/sola-scriptura)

## Checking translations of Holy Scripture

- [Checking scripture translations](/topics/scripture/checking-translations)

- External link: [Strengths and weaknesses of the Douay Rheims](https://www.catholicculture.org/culture/library/view.cfm?recnum=4300)
