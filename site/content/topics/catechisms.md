+++
title = "Catechisms of the Catholic Church"
+++

# What's a catechism?

# Universal Catechism

If you aren't sure what you're looking for, this is probably what you want:
[Catechism of the Catholic Church](http://www.usccb.org/beliefs-and-teachings/what-we-believe/catechism/catechism-of-the-catholic-church/)

A handy [table of contents with paragraph numbers](http://scborromeo.org/ccc/ccc_toc2.htm) for the universal catechism.


# National Catechism

http://www.usccb.org/beliefs-and-teachings/what-we-believe/catechism/us-catholic-catechism-for-adults/

Not recommended.


# Catechism for children, aka YOUCAT

These are very commonly used.

- https://www.youcat.org/products/youcat-for-kids/

- https://www.youcat.org/products/youcat


# Baltimore Catechism

https://en.wikipedia.org/wiki/Baltimore_Catechism

[1941 Baltimore Catechism](https://www.catholicity.com/baltimore-catechism/)

[No 4, an explanation of the Baltimore Catechism for catechists](https://www.gutenberg.org/ebooks/14554)
