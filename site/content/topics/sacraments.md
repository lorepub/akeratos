+++
title = "Sacraments"
+++

# Seven Sacraments

## Sacraments of Initiation

- [Baptism](/topics/sacraments/baptism)

- [Confirmation](/topics/sacraments/confirmation)

- [Eucharist](/topics/sacraments/eucharist)

## Sacraments of Healing

- [Reconciliation, also called confession](/topics/sacraments/confession)

- [Anointing of the sick](/topics/sacraments/anointing-of-the-sick), also called extreme unction. Spuriously called "last rites."

## Sacraments of service

- [Matrimony](/topics/sacraments/matrimony)

- [Holy Orders](/topics/sacraments/holy-orders)
