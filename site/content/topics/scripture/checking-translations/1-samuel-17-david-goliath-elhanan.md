+++
title = "Checking translations of 2 Samuel 21:19 and 1 Chronicles 20:5"
+++

[Return to checking translations](/topics/scripture/checking-translations)

<br>

This article written with gratitude for Verbum's "Who Killed Goliath" slides on textual-critical analysis of the relevant verses.

### Why this passage matters

The verse about "Elhanan killing Goliath" in 2 Samuel 21:19 seems to contradict the account of David killing Goliath in 1 Samuel 17. 1 Chronicles 20:5 might be the same account but without a scribal error.

### Translations

#### RSV2CE

##### 2 Samuel 21:19

And there was again war with the Philistines at Gob; and Elhanan the son of Jaareoregim, the Bethlehemite, slew Goliath the Gittite, the shaft of whose spear was like a weaver’s beam.

##### 1 Chronicles 20:5

And there was again war with the Philistines; and Elhanan the son of Jair slew Lahmi the brother of Goliath the Gittite, the shaft of whose spear was like a weaver’s beam.

#### ESVCE

##### 2 Samuel 21:19

And there was again war with the Philistines at Gob, and Elhanan the son of Jaare-oregim, the Bethlehemite, struck down Goliath the Gittite, the shaft of whose spear was like a weaver’s beam.

##### 1 Chronicles 20:5

And there was again war with the Philistines, and Elhanan the son of Jair struck down Lahmi the brother of Goliath the Gittite, the shaft of whose spear was like a weaver’s beam.

#### NABRE

##### 2 Samuel 21:19

There was another battle with the Philistines, in Gob, and Elhanan, son of Jair from Bethlehem, killed Goliath of Gath, whose spear shaft was like a weaver’s beam.

##### 1 Chronicles 20:5

There was another battle with the Philistines, and Elhanan, the son of Jair, slew Lahmi, the brother of Goliath of Gath, whose spear shaft was like a weaver’s beam.

#### NIV

##### 2 Samuel 21:19

In another battle with the Philistines at Gob, Elhanan son of Jair the Bethlehemite killed the brother of Goliath the Gittite, who had a spear with a shaft like a weaver’s rod.

##### 1 Chronicles 20:5

In another battle with the Philistines, Elhanan son of Jair killed Lahmi the brother of Goliath the Gittite, who had a spear with a shaft like a weaver’s rod.

#### KJV

##### 2 Samuel 21:19

And there was again a battle in Gob with the Philistines, where Elhanan the son of Jaare-oregim, a Beth-lehemite, slew the brother of Goliath the Gittite, the staff of whose spear was like a weaver’s beam.

##### 1 Chronicles 20:5

And there was war again with the Philistines; and Elhanan the son of ﻿Jair slew Lahmi the brother of Goliath the Gittite, whose spear staff was like a weaver’s beam.

#### Douay-Rheims

##### 2 Samuel 21:19

And there was a third battle in Gob against the Philistines, in which Adeodatus the son of the Forrest an embroiderer of Bethlehem slew Goliath the Gethite, the shaft of whose spear was like a weaver’s beam. 

##### 1 Chronicles 20:5

Another battle also was fought against the Philistines, in which Adeodatus the son of Saltus a Bethlehemite slew the brother of Goliath the Gethite, the staff of whose spear was like a weaver’s beam.

#### Vulgate

##### 2 Samuel 21:19

Tertium quoque fuit bellum in Gob contra Philisthæos, in quo percussit Adeodatus filius Saltus polymitarius Bethlehemites Goliath Gethæum, cujus hastile hastæ erat quasi liciatorium texentium.

##### 1 Chronicles 20:5

Aliud quoque bellum gestum est adversus Philisthæos, in quo percussit Adeodatus filius Saltus Bethlehemites fratrem Goliath Gethæi, cujus hastæ lignum erat quasi liciatorium texentium.

#### JPS Bible

##### 2 Samuel 21:19

And there was again war with the Philistines at Gob; and Elhanan the son of Jaare-oregim the Beth-lehemite slew Goliath the Gittite, the staff of whose spear was like a weaver’s beam.

##### 1 Chronicles 20:5

And there was again war with the Philistines; and Elhanan the son of Jair slew Lahmi the brother of Goliath the Gittite, the staff of whose spear was like a weaver’s beam.

#### Hebrew

##### 2 Samuel 21:19

וַתְּהִי־ע֧וֹד הַמִּלְחָמָ֛ה בְּג֖וֹב עִם־פְּלִשְׁתִּ֑ים וַיַּ֡ךְ אֶלְחָנָן֩ בֶּן־יַעְרֵי֙ אֹרְגִ֜ים בֵּ֣ית הַלַּחְמִ֗י אֵ֚ת גָּלְיָ֣ת הַגִּתִּ֔י וְעֵ֣ץ חֲנִית֔וֹ כִּמְנ֖וֹר אֹרְגִֽים׃ ס

##### 1 Chronicles 20:5

וַתְּהִי־ע֥וֹד מִלְחָמָ֖ה אֶת־פְּלִשְׁתִּ֑ים וַיַּ֞ךְ אֶלְחָנָ֣ן בֶּן־יָעוּר אֶת־לַחְמִי֙ אֲחִי֙ גָּלְיָ֣ת הַגִּתִּ֔י וְעֵ֣ץ חֲנִית֔וֹ כִּמְנ֖וֹר אֹרְגִֽים׃

#### Leiden Peshitta

Ed. note: please let me know if you know anyone that can read Syriac.

##### 2 Samuel 21:19

ܘܗܘܐ ܬܘܒ ܩܪܒܐ ܠܐܝܣܪܝܠ ܥܡ ܦܠܫܬ̈ܝܐ ܘܩܛܠ ܐܠܚܝܢ ܒܪ ܡܠܦ ܙܩܘܪܐ ܕܡܢ ܒܝܬ ܠܚܡ ܠܓܘܠܝܬ ܦܠܫܬܝܐ ܘܫܘܝܐ ܕܢܝܢܟܗ ܥܒܐ ܗܘܐ ܐܝܟ ܢܘܠܐ ܕܓܪ̈ܕܝܝܐ 

##### 1 Chronicles 20:5

ܕܡܢ ܒܢ̈ܝܐ ܕܓܕܦܐ ܡܚܣܕܢܝܬܐ ܕܗܘܐ ܐܚܘܗܝ ܕܓܘܠܝܕ ܓܢܒܪܐ ܕܡܢ ܓܬ ܘܫܢܢܐ ܕܢܝܙܟܗ ܥܒܐ ܗܘܐ ܐܝܟ ܢܘܠܐ ܕܓܪ̈ܕܚܐ

#### Septagint

##### 2 Samuel 21:19

καὶ ἐγένετο ὁ πόλεμος ἐν Γοβ μετὰ τῶν ἀλλοφύλων. καὶ ἐπάταξεν Ελεαναν υἱὸς Αριωργιμ ὁ Βαιθλεεμίτης τὸν Γολιαθ τὸν Γεθθαῖον, καὶ τὸ ξύλον τοῦ δόρατος αὐτοῦ ὡς ἀντίον ὑφαινόντων.

##### 1 Chronicles 20:5

καὶ ἐγένετο ἔτι πόλεμος μετὰ τῶν ἀλλοφύλων. καὶ ἐπάταξεν Ελλαναν υἱὸς Ιαϊρ τὸν Λεεμι ἀδελφὸν Γολιαθ τοῦ Γεθθαίου, καὶ ξύλον δόρατος αὐτοῦ ὡς ἀντίον ὑφαινόντων.

### Comments

Elhanan and the Death of Goliath

Robert Chisholm offers four possible solutions to this apparent contradiction:

1. identifying David with Elhanan;

2. asserting that Goliath is a title and not a proper name;

3. regarding 2 Sam 21:19 as textually corrupt and using the companion passage in 1 Chr 20:5 as a way to reconstruct the text;

4. regarding 2 Sam 21:19 as correct and that the name Goliath was supplied to an unnamed champion that David slew in 1 Sam 17.

Chisholm was inclined toward the latter two options.

Kaspars Ozolins has [a great article on this at Text and Canon](https://textandcanon.org/who-really-killed-goliath/). His article gets into a good amount of detail on this issue. I think this is the best argument for the "brother" variation.

#### My thoughts

I think the KJV/NIV translation where 2 Samuel 21:19 says "brother of Goliath" is more accurate, but this is a tough case. Justifying it requires looking at context, judging it against the apparent contradiction, and comparing 2 Samuel 21:19 against 1 Chronicles 20:5. I think the case for a scribal error is good and that Kaspars' article makes a good case for it. I'm made somewhat uncertain by broad the adoption of the literal translation. I think there must be a good reason for this and I don't feel like I've seen a defense of the translation that the RSV/ESV/etc. go with. If anyone knows of one, could you please send it to me? I'll be researching and commenting on this issue more soon.

Incidentally, this is the only case I've found so far where the KJV varies and I agree with the KJV. I'd like to note that the KJV was supposedly translated from the Masoretic texts which do _not_ include the word brother in any version I'm aware of.

Another question I have is how many fragments 2 Samuel 21:19 shows up in. I checked a couple in [Verbum](https://verbum.com/) and couldn't find it. I need to figure out how to find verses across all my manuscripts in my library.

I asked [Luis Dizon](https://twitter.com/luisdizon) on Twitter and he graciously linked his Reason & Theology podcast episode where he discussed 2 Samuel and 1 Chronicles. [Here is the episode he linked](https://www.youtube.com/watch?v=s3gnkOUDMO4&t=1310s), timestamped for when he begins talking about 2 Samuel 21:19 specifically. You should watch the R&T episode but the executive summary is that Dizon believes a conjectural emendation is needed here, agreeing with the article by Kaspars Ozolins.

Wikipedia discusses this issue and mentions:

>Targum, a translation of the Hebrew scriptures into Aramaic, identified Elhanan with David as both were from Bethlehem (targum 2 Samuel 21:19), although this creates yet another problem in that Elhanan is listed as one of David's followers and the killings occur in different places.

There's also the issue that 1 Chronicles 20:5 could've been corrupted from an original identical verse that it had in common with 2 Samuel 21:19. This would lead to the assumption that the giant killed by Elhanan was actually unnamed and that "Lahmi" is a corruption of Bethlehemite. The original rendering suggested by Ozolins is:

>and Elhanan the son of Jaur, the Bethlehemite, struck down the brother of Goliath the Gittite, the shaft of whose spear was like a weaver’s beam

Following the account of Elhanan's victory in both 2 Samuel and 1 Chronicles is a victory on the part of Jonathan the son of Shimei. The giant goes unnamed in both versions. However, the two previous giants mentioned being killed were both named. Hard for me to say if that means much. The reason Ozolins suggests the above rendering is that there are scribal errors that could lead to both the 2 Samuel and the 1 Chronicles versions.

There's also an odd Jewish tradition that Elhanan was actually David. The Targum takes this route to resolving the apparent contradiction.

Kaspars Ozolins more in depth paper concludes that the gentilic (the Bethelehemite) was probably in the original text common to both verses. The gentilic is rare in the Hebrew bible but it occurs exclusively in Samuel. Ozolins notes them as occurring in 1 Sam 16:1, 18, 17:58 and 2 Sam 21:19.

Tough case, but there's no contradiction here. Just difficult to unwind copy errors.

#### Is this conclusion permissible to a Catholic?

Yeah, we believe the scriptures were inerrant as originally delivered. We weren't promised 2,000 years of perfect copying and that is manifestly not the case here or elsewhere as it concerns biblical manuscripts. There are no perfect or "pure" manuscripts for scripture, this is just the work that has to be done to understand what is available to us.

This method of analyzing the text is "lower" textual criticism along the lines given approbation in Pius XII's Divino afflante Spiritu.

### Bible translations that include brother

From: http://textus-receptus.com/wiki/Article:_2_Samuel_21:19_Who_Killed_Goliath%3F_by_Will_Kinney

>As far as the word "brother" being "added" to the text, the KJB is not at all alone in this. So does the 1936 Hebrew Publishing Company version, the Hebrew Names Version, the World English Bible, the 1998 New International Readers Version put out by the same people who brought us the NIV, the 2001 Easy-to-Read Version, the 1996 New Living Translation, Young's 'literal' version, Webster's 1833, the Natural Israelite Bible English Version 2008, the Ancient Roots Translinear Bible 2009, the Heritage Bible 2003, the 2001 Urim-Thummin Version, the Word of Jah version 1993, the Third Millenium bible, 1998 and the NKJV 1982.

>Foreign language bibles that also tell us that Elhanan killed THE BROTHER OF Goliath are the French Martin bible 1744, the Italian New Diodati 1991 -” ed Elhanan, figlio di Jaare-Oreghim di Betlemme uccise IL FRATELLO DI Goliath di Gath”, the Portuguese O Livro 2000 put out by the same publishers who bring us the NIV , the International Bible Society. The 2000 O Livro reads - “El-Hanã matou O IRMAO de Golias o giteu”; Lamsa’s 1936 translation of the Syriac Peshitta, and the MODERN Greek translation of the Old Testament.

A note on Young's Literal Version including "brother", the version is noted for strictly following something like formal equivalence but it interpolated the word brother in 2 Samuel 21:19 anyhow.
