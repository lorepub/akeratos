+++
title = "Checking translations of Genesis 3:16b"
+++

[Return to checking translations](/topics/scripture/checking-translations)

<br>

### Why this passage matters

This verse has been a bone of contention in the debates between people holding egalitarian or complementarian perspectives on Christian marriage. Susan T. Foh's "What is the Woman's Desire?" undergirded the fundamentalist exegesis of this verse for quite some time in the Anglophone world. There's also some debate on whether it is prescriptive or descriptive.

### Translations

#### RSV2CE

yet your desire shall be for your husband,
and he shall rule over you.” 

#### ESV

Your desire shall be contrary to﻿ your husband,
but he shall rule over you.” 

#### Orthodox Study Bible

Your recourse will be to your husband, and he shall rule over you."

#### NABRE

Yet your urge shall be for your husband,
and he shall rule over you. 

#### NIV

Your desire will be for your husband,
and he will rule over you.” 

#### KJV

and thy desire shall be ﻿to thy husband, and he shall ﻿rule over thee.

#### Vulgate

in dolore paries filios, et sub viri potestate eris, et ipse dominabitur tui.

#### Hebrew

וְאֶל־אִישֵׁךְ֙ תְּשׁ֣וּקָתֵ֔ךְ וְה֖וּא יִמְשָׁל־בָּֽךְ׃ ס

#### Septagint

καὶ πρὸς τὸν ἄνδρα σου ἡ ἀποστροφή σου, καὶ αὐτός σου κυριεύσει.

### Comments on Genesis 3:16b

Since the release of the ESV 2016 edition titled, "Permanent Text", the translation has gone with "contrary to" for the preposition אֶל. I'm not a Hebraist so I'm not equipped to evaluate the likelihood "contrary" fits the use of the preposition here. HALOT supposedly indicates the "against" in el' is locative or adjacent to something. "Contrary" doesn't seem justified here.

I will say that it's usually the safer/better bet to go with the more open-ended translation of words that doesn't close off possible interpretations for the reader, especially with multi-purpose prepositions like el'. I prefer the more traditional for/to/toward translations to the "contrary to" in the 2016 ESV.

When the Hebrew is ambiguous it's usually safer to let the English be similarly ambiguous in meaning rather than narrowing the words.

The Septuagint here is pretty interesting. The conjunction and preposition get translated pretty literally from the Hebrew, "pros" from el'. The definitions in [Liddell & Scott](http://www.perseus.tufts.edu/hopper/text?doc=Perseus:text:1999.04.0057:entry=pro/s) includes from, toward, and before. pros can be of place, of effects proceeding a cause, dependence or close connection, and derivation. In the dative, pros expresses proximity, presence, motion towards or against, clinging closely, close engagement, and union or addition. Naively, I'd say there's nothing to indicate pros isn't indicating to/toward/togetherness with the husband.

An interesting word here is αποστροφή or apostrofi/apostrophe. This same word also occurs in:

- Sir 16:30	Its surface he covered with every kind of living creature which must return into it again.

- Sir 18:24: Think of wrath on the day of death, the time of vengeance when he will _hide_ his face.

- Sir 41:21 of _rebuffing_ your own relatives; Of defrauding another of his appointed share, Of gazing at a man’s wife,

- 1 Clement 4:5 Be at peace: thine offering _returns_ to thyself, and thou shalt again possess it.

Some good translations for apostrophe could be "turn away", recourse, or return. This is the LXX translation of תשוקה or teshuqah, which has historically been translated into English as desire. Recourse, escape, resort, resource or means for getting water seems most probable for the intent of the Septuagint's authors. The Orthodox Study Bible's translation of apostrophe in Genesis 3:16b goes with the translation recourse.

### Related verses

#### wa-el | el / "to, toward, for, against"

- Genesis 3:19, and to dust
- Genesis 4:8, rose up against

#### tesuqah / "desire"

- Genesis 4:7, and its desire
- Songs 7:10, his desire is for me

<!-- In “The Meaning of Hebrew תשׁוקה,” Journal of Semitic Studies 61 (2016):365-387, Andrew A. Macintosh argues that tesuqah is better understood to mean devotion, preoccupation, focus, or concern. I'm not sure how well that squares with Songs 7:10 but it's an interesting idea. -->

<!-- Susan T. Foh in "What is the Woman's Desire?" digs into the underlying meaning of tesuqah and how it's used in the bible. Her interpretation of Genesis 3:16b became standard among Protestant Christians in the Anglophone world for some time. There are some issues with her arguments. -->

<!-- It is my personal opinion that this passage is descriptive rather than prescriptive. -->

<!-- #### tesuqah / apostrophe -->

<!-- - 1 Peter 3:1 -->

<!-- RSV2CE: -->

<!-- 16 and keep your conscience clear, so that, when you are abused, those who revile your good _behavior_ in Christ may be put to shame. -->

<!-- LXX: -->
<!-- Ὁμοίως γυναῖκες ὑποτασσόμεναι τοῖς ἰδίοις ἀνδράσιν, ἵνα καὶ εἴ τινες ἀπειθοῦσιν τῷ λόγῳ διὰ τῆς τῶν γυναικῶν _ἀναστροφῆς_ ἄνευ λόγου κερδηθήσονται -->

<!-- ἀναστροφῆς / anastrophe: conduct, behavior, way of life -->
