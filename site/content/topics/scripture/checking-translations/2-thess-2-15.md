+++
title = "Checking translations of 2 Thessalonians 2:15"
+++

[Return to checking translations](/topics/scripture/checking-translations)

<br>

### Why this passage matters

There is a tendency among some Christian traditions to deny the role of tradition in Christian practice and belief.

### Translations

#### RSV2CE

So then, brethren, stand firm and hold to the traditions which you were taught by us, either by word of mouth or by letter.

#### ESV

So then, brothers, stand firm and hold to the traditions that you were taught by us, either by our spoken word or by our letter.

#### NKJV / Orthodox Study Bible

Therefore, breathren, stand fast and hold the traditions which you were taught, whether by word or our epistle.

#### NABRE

Therefore, brothers, stand firm and hold fast to the traditions that you were taught, either by an oral statement or by a letter of ours.

#### KJV

Therefore, brethren, stand fast, and hold the traditions which ye have been taught, whether by word, or our epistle.

#### NIV

So then, brothers and sisters, stand firm and hold fast to the teachings we passed on to you, whether by word of mouth or by letter.

#### Vulgate

itaque fratres state et tenete <strong>traditiones</strong> quas didicistis sive per sermonem sive per epistulam nostram

#### Greek

Ἄρα οὖν, ἀδελφοί, στήκετε καὶ κρατεῖτε τὰς <strong>παραδόσεις</strong> ἃς ἐδιδάχθητε εἴτε διὰ λόγου εἴτε δι’ ἐπιστολῆς ἡμῶν.

### Comments on 2 Thessalonians 15

This verse is often brought up in the context of Sola Scriptura debates. Usually the emphasis is on the word _παραδόσεις_ and whether it ought be translated "traditions" or "teachings" in English. The etymology of the word tradition in English derives from the Latin _traditionem_, which is variously "delivering up, surrender, a handing down, or a giving up." It's the word used in Latin for what we call tradition because of the successive "handing down" involved in the cultural or religious transfer of a tradition.

[_παραδόσεις_](https://en.wiktionary.org/wiki/%CF%80%CE%B1%CF%81%CE%B1%CE%B4%CF%8C%CF%83%CE%B5%CE%B9%CF%82) is the nominative/accusative/vocative plural of [_παράδοσις_](https://en.wiktionary.org/wiki/%CF%80%CE%B1%CF%81%CE%AC%CE%B4%CE%BF%CF%83%CE%B9%CF%82#Ancient_Greek). The translations offered are:

- transfer
- granting, bestowal
- giving up, surrender
- tradition

What's striking here is not just that tradition is specifically called out but that it has the same alternative uses as _traditionem_ in Latin. It seems clear that the appropriate translation is _tradition_ and not _teachings_. When scripture means teachings, it uses [_διδάσκω_](https://biblehub.com/greek/1321.htm) and quite liberally: it appears about 220 times in the New Testament.

However, what could be missed in the prooftexting of the "traditions" in 2 Thessalonians is the end of the verse:

>either by word of mouth or by letter.

Scripture itself does not give precedence or primacy to scripture here but to traditions communicated by the spoken word and by written letters.
