+++
title = "Checking translations of Isaiah 7:14"
+++

[Return to checking translations](/topics/scripture/checking-translations)

<br>

### Why this passage matters

Religious Jews and some revisionist scholars will often deny the Messianic or Christological signs in the Old Testament, especially any that point more clearly to Jesus Christ rather than a yet-to-come messiah. Christological signs that indicate divinity are usually more often objected to.

### Translations

#### RSV2CE

Therefore the Lord himself will give you a sign. Behold, a young woman shall conceive and bear a son, and shall call his name Imman′u-el.

#### ESV

Therefore the Lord himself will give you a sign. Behold, the virgin shall conceive and bear a son, and shall call his name Immanuel.

#### Orthodox Study Bible

Therefore the Lord Himself will give you a sign: behold, the virgin shall conceive and bear a Son, and you shall call His name Immanuel.

#### NABRE

Therefore the Lord himself will give you a sign; the young woman, pregnant and about to bear a son, shall name him Emmanuel.

#### NIV

Therefore the Lord himself will give you a sign: The virgin will conceive and give birth to a son, and will call him Immanuel.

#### KJV

Therefore the Lord himself shall give you a sign; Behold, a virgin shall conceive, and bear a son, and shall call his name Immanuel.

#### Vulgate

Propter hoc dabit Dominus ipse vobis signum: ecce virgo concipiet, et pariet filium, et vocabitur nomen ejus Emmanuel.

<!-- #### Hebrew -->

### Comments on Isaiah 7:14

This verse's use of "almah", translated in most places as virgin, is surprisingly controversial even among many Christians. The contention is that it means young woman but biblical Hebrew being what it is, it's not as simple as that. A non-virgin young woman getting pregnant and bearing a child isn't a prophecy of salvation, it's quotidian.

Further, the fuller Hebrew for the woman here is "ha'almah", "ha" is the definite article here, so "the virgin." Biblical Hebrew has "bethulah" for virgin as well but there are examples in Greek and Hebrew of both words being used to indicate age or virginity.

The Septuagint, a translation written by Jews a few centuries before the birth of Christ, unambiguously uses the Greek for virgin here. The Orthodox Study Bible's translation of Isaiah 7:14 reflects that.
