+++
title = "Checking translations of 2 Chronicles 13:9"
+++

[Return to checking translations](/topics/scripture/checking-translations)

<br>

### Why this passage matters

It doesn't have tremendous theological impact but it highlights how subtle translating Hebrew can be.

### Translations

#### RSV2CE

Have you not driven out the priests of the Lord, the sons of Aaron, and the Levites, and made priests for yourselves like the peoples of other lands? Whoever comes to consecrate himself with a young bull or seven rams becomes a priest of what are no gods.

#### ESV

Have you not driven out the priests of the Lord, the sons of Aaron, and the Levites, and made priests for yourselves like the peoples of other lands? Whoever comes for ordination﻿ with a young bull or seven rams becomes a priest of what are not gods.

#### Orthodox Study Bible

Did you not cast out the priests of the Lord, the sons of Aaron, and the Levites, and made priests for yourselves from the peoples of the land so that whoever comes to consecrate himself with a young bull and seven rams may be a priest of things that are not gods?

#### NABRE

Have you not expelled the priests of the Lord, the sons of Aaron, and the Levites, and made for yourselves priests like the peoples of other lands? Everyone who comes to consecrate himself with a young bull and seven rams becomes a priest of no-gods.

#### KJV

Have ye not cast out the priests of the Lord, the sons of Aaron, and the Levites, and have made you priests after the manner of the nations of other lands? so that whosoever cometh ﻿to consecrate himself with ﻿a young bullock and seven rams, the same may be a priest of them that are ﻿no gods.

#### NIV

But didn’t you drive out the priests of the Lord, the sons of Aaron, and the Levites, and make priests of your own as the peoples of other lands do? Whoever comes to consecrate himself with a young bull and seven rams may become a priest of what are not gods.

#### Vulgate

Et ejecistis sacerdotes Domini, filios Aaron, atque Levitas, et fecistis vobis sacerdotes sicut omnes populi terrarum: quicumque venerit, et initiaverit manum suam in tauro de bobus, et in arietibus septem, fit sacerdos eorum qui non sunt dii.

#### Septuagint

ἦ οὐκ ἐξεβάλετε τοὺς ἱερεῖς κυρίου τοὺς υἱοὺς Ααρων καὶ τοὺς Λευίτας καὶ ἐποιήσατε ἑαυτοῖς ἱερεῖς ἐκ τοῦ λαοῦ τῆς γῆς; πᾶς ὁ προσπορευόμενος πληρῶσαι τὰς χεῖρας ἐν μόσχῳ ἐκ βοῶν καὶ κριοῖς ἑπτὰ καὶ ἐγίνετο εἰς ἱερέα τῷ μὴ ὄντι θεῷ.

### Comments on 2 Chronicles 13:9

This [thread from the Biblical Hermeneutics Stack Exchange](https://hermeneutics.stackexchange.com/questions/22219/is-2-chronicles-139-translated-incorrectly-in-the-rsv-nrsv-esv) ([Archive.org link](https://web.archive.org/web/20210227221737/https://hermeneutics.stackexchange.com/questions/22219/is-2-chronicles-139-translated-incorrectly-in-the-rsv-nrsv-esv)) is thorough. I found the thread convincing as to how the waw in the Hebrew between the bull and the seven rams should be translated.

I'd say that I fall on the side of the waw being best translated "and" rather than "or" on the basis of how rare "or" is the best sense of waw, how the waw was translated in the Vulgate and Septuagint, the connection with Exodus 29:1, and on the authority of the Hebrew experts cited.

My guess the ESV translators' intent is that they were making Abijah's jest more apparent to the English reader.
