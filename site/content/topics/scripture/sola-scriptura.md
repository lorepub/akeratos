+++
title = "Sola scriptura or scripture alone"
+++

## What is Sola Scriptura?

On [Wikipedia](https://en.wikipedia.org/wiki/Sola_scriptura) Sola Scriptura is defined as, "that the Christian scriptures are the sole infallible source of authority for Christian faith and practice."

This is a generally agreed upon definition that should work as well for high church Anglicans as fundamentalist congregationalist sects.

## What does scripture say about Sola Scriptura?

### 2 Thessalonians 2:15

>So then, brethren, stand firm and hold to the traditions which you were taught by us, either by word of mouth or by letter.

[How 2 Thessalonians 2:15 is translated](/topics/scripture/checking-translations/2-thess-2-15)

### 2 Thessalonians 3:6

>Now we command you, brethren, in the name of our Lord Jesus Christ, that you keep away from any brother who is living in idleness and not in accord with the tradition that you received from us.

### 1 Corinthians 10:4

>and all drank the same supernatural drink. For they drank from the supernatural Rock which followed them, and the Rock was Christ.

St. Paul is talking about the people of Israel following the _Rock_ from which they took supernatural or spiritual drink. The word for Rock here is [_	πέτρα_](https://biblehub.com/greek/petra_4073.htm).

### Matthew 16:18

>And I tell you, you are Peter, and on this rock I will build my church, and the powers of death shall not prevail against it.

The words for Peter and rock here are _Πέτρος_ and [_πέτρᾳ_](https://biblehub.com/greek/petra_4073.htm), they're the same word.

"Powers of death" in the Greek is "the gates of Hades."

## Scripture cited in favor of Sola Scriptura

### Matthew 15:3,6 and Mark 7:7

[I strongly recommend Haydock's commentary on Matthew 15.](https://www.studylight.org/commentaries/eng/hcc/matthew-15.html)

Briefly, the issue here is the Pharisees, the predecessors of present-day rabbinical Judaism, using a tradition they concocted to get out of obeying God's law. Specifically, that of honoring thy father and mother. The pharisees and their followers were using corban (korban or [_κορβᾶν_](https://biblehub.com/greek/2878.htm)) to get out of caring for their needy parents.

Jesus isn't accusing the idea of traditions, but that they cannot take precedence over God's law.
