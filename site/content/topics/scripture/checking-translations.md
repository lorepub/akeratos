+++
title = "Checking translations of Holy Scripture"
+++

[Return to scriptural topics](/topics/scripture/)

<br>

It's important to know where a translation of scripture might be untrustworthy or obscure the meaning of the original Greek texts. Absent working directly from a [Nestle-Aland](https://www.academic-bible.com/en/bible-society-and-biblical-studies/scholarly-editions/greek-new-testament/nestle-aland/), you'll want to check certain texts against the Greek.

Here's some specific verses I've found helpful for understanding where the biases in a particular translation might be.

- [Genesis 3:16b, on spousal relations](/topics/scripture/checking-translations/genesis-3-16b)

- [2 Samuel 21:19, and 1 Chronicles 20:5; David, Elhanan, and Goliath](/topics/scripture/checking-translations/1-samuel-17-david-goliath-elhanan)

- [2 Chronicles 13:9](/topics/scripture/checking-translations/2-chron-13-9)

- [Isaiah 7:14](/topics/scripture/checking-translations/isaiah-7-14)

- [Matthew 19:17](/topics/scripture/checking-translations/matt-19-17)

- [2 Thessalonians 2:15](/topics/scripture/checking-translations/2-thess-2-15)

## Okay but which English translation should I use?

I typically use [RSV2CE in the form of the Great Adventure bible](https://ascensionpress.com/products/the-great-adventure-catholic-bible).

I wouldn't say the RSV is flawless but it has been reliable enough for me. I'm still evaluating other verses and texts to put forth a more complete summary of the translations. I do tend to favor translations that strive for formal equivalence. The ESVCE is close but I haven't found any differences that seem like an improvement yet. I'm open to discovering verses where ESV does a better job with the accuracy or artfulness of the text.

## Translations used in the articles

Unless otherwise stated, the following labels refer to these translations of the Bible:

- **RSVCE** or **RSV2CE**: The Holy Bible, Revised Standard Version; Second Catholic Edition. (San Francisco: Ignatius Press, 2006)

- **ESVCE**: English Standard Version Catholic Edition (n.p.: Augustine Institute, 2019)

- **NAB** or **NABRE**: New American Bible, Revised Edition. (Washington, DC: The United States Conference of Catholic Bishops, 2011)

- **NIV**: The New International Version (Grand Rapids, MI: Zondervan, 2011), 2 Sa 21:19.

- **Douay Rheims**: The Holy Bible, Translated from the Latin Vulgate (Bellingham, WA: Logos Bible Software, 2009)

- **Vulgate**: Biblia Sacra Juxta Vulgatam Clementinam., Ed. electronica. (Bellingham, WA: Logos Bible Software, 2005)

- **JPS**: Jewish Publication Society of America, Torah Nevi’im U-Khetuvim. The Holy Scriptures according to the Masoretic Text. (Philadelphia, PA: Jewish Publication Society of America, 1917), 2 Sa 21:19.

- **KJV**: The Holy Bible: King James Version, Electronic Edition of the 1900 Authorized Version. (Bellingham, WA: Logos Research Systems, Inc., 2009), 2 Sa 21:19.

- **Septuagint**: Septuaginta: With Morphology, electronic ed. (Stuttgart: Deutsche Bibelgesellschaft, 1979), 2 Kgdms 21:19.

- **Hebrew**: Christo van der Merwe, The Lexham Hebrew-English Interlinear Bible (Bellingham, WA: Lexham Press, 2004), 2 Sa 21:19. The manuscript is the Biblia Hebraica Stuttgartensia or BHS.

<!-- there's a reason there was a push among evangelical Christians to develop the ESV, which is usually their preferred translation that emphasizes formal equivalence. -->

<!-- NJB -->

<!-- "Your yearning will be for your husband, and he will dominate you." -->

<!-- "The Lord will give you a sign in any case: It is this: the young woman is with child and will give birth to a son whom she will call Immanuel. " -->

<!-- "Stand firm, then, brothers, and keep the traditions that we taught you, whether by word of mouth or by letter." -->

<!-- <h2 id="TEMPLATE">TEMPLATE</h2> -->

<!-- ### Translations -->

<!-- #### RSV2CE -->

<!-- #### ESV -->

<!-- #### NABRE -->

<!-- #### NIV -->

<!-- #### KJV -->

<!-- #### Vulgate -->

<!-- #### Greek -->

<!-- ### Comments on TEMPLATE -->
