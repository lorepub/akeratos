+++
title = "Evolution"
+++

# Thomistic Institute

Remember that these are more academic and speculative investigations of this topic. You aren't obligated to believe a particular framing of this issue as a Catholic.

[Defending Adam after Darwin](https://soundcloud.com/thomisticinstitute/defending-adam-after-darwin-fr-nicanor-austriaco-op)
