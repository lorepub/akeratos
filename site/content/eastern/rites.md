+++
title = "Rites of the Eastern churches"

[extra]
notitle = true
+++

## Rites of the Eastern Christian churches

- Byzantine rite{{ note_ref(label="byzantine-rite") }}, which is the ritual of the majority of Eastern Catholics and the *vast* majority of Eastern Orthodox, excepting the Western rite.
  * Recensio Vulgata also called Nikonian.
    {{ note_ref(label="recensio-vulgata") }} Nikonian refers to the reforms of Patriarch Nikon. {{ note_ref(label="nikon") }}
    - Covers roughly these churches:
      - Russian Orthodox Church
      - Bulgarian Orthodox Church
        * Bulgarians do not strictly follow Nikonian usage. The rubrics and Holy Week in particular tend to follow Constantinopolitan use.
        {{ note_ref(label="bulgarian-variety") }}
    - A paper on the compilation of the Russian-Byzantine Catholic Menologion
      {{ note_ref(label="recensio-vulgata-menologion") }}
  * Recensio Ruthena or Ruthenian recension. "Ruthenian" is an exonym that covers a broad swath of slavic peoples. {{ note_ref(label="ruthenians") }}
    - Used by the Ukrainian, Rusyn (Ruthenian), Slovak, Hungarian, and Croatian Greek Catholic Churches
    - The Ruthenian recension properly refers to the liturgical books produced in Rome from 1940-1973. {{ note_ref(label="ruthenian-use") }} The project's main purpose was to purify the divine liturgy of Latinization. The Ruthenian liturgy was selected primarily from pre-Nikonian and Nikonian sources.
    - Communities that use the Ruthenian recension tend to follow Greek use more than Nikonian, especially in the para-liturgical accoutrements.
      {{ note_ref(label="recensio-ruthena-greek") }}
  * Italo-Greek is the typical name for the use of the Byzantine Italo-Greek-Albanian Church.

## References

{% note(
    label="recensio-vulgata",
    url="https://en.wikipedia.org/wiki/Byzantine_Rite") %}
The Wikipedia page on the Byzantine rite.
{% end %}

{% note(
label="recensio-vulgata",
url="https://www.vaticanum.com/en/divina-liturgia-s-patris-nostri-iohannis-chrysostomi-ediz-vulgata-ediz-slava") %}
Example of a copy of the Nikonian recension or recensio vulgata
{% end %}

{% note(
label="nikon",
url="https://en.wikipedia.org/wiki/Patriarch_Nikon_of_Moscow") %}
The Wikipedia page on Patriarch Nikon.
{% end %}

{% note(
label="bulgarian-variety",
url="https://www.byzcath.org/forums/ubbthreads.php/topics/304353/re-recensions#Post304353") %}
Edward Yong explains that Bulgarian Orthodox do not strictly follow Nikonian use of the Byzantine rite.
{% end %}

{% note(
label="recensio-vulgata-menologion", url="http://slovene.ru/2021_1_Semenenko-Basin_Caprio.pdf") %}
"Russian Liturgical Memories in the Slavic Byzantine Catholic Menologion (Recensio Vulgata) of the Mid-20th Century."
{% end %}

{% note(
label="ruthenians",
url="https://en.wikipedia.org/wiki/Ruthenians", description="") %}
The Wikipedia page on Ruthenians.
{% end %}

{% note(
label="ruthenian-use", url="http://forums.orthodoxchristianity.net/threads/ruthenian-recension.57968/post-1112241") %}
An Orthodox deacon giving some history of the compilation of the Ruthenian recension.
{% end %}

{% note(
label="recensio-ruthena-greek", 
url="https://www.byzcath.org/forums/ubbthreads.php/topics/209135/re-ruthenian-rescension#Post209135") %}
A deacon describing Ruthenian/Ukrainian Greek Catholic communities following Greek style more often than not.
{% end %}

<!-- Ruthenian differences -->
<!-- The differences are often subtle but the ones that I can remember of the top of my head: -->
<!-- Ruthenian Vespers: -->
<!-- 8 Prayers of Light -->
<!-- Unique Entrance Prayer -->
<!-- Ruthenian Liturgy -->
<!-- Use of Paschal Antiphons on Sundays -->
<!-- Litany of Fervent Supplication has fewer petitions -->
<!-- Great Entrance petitions are done differently -->
<!-- No Troparia at Epiclesis -->
<!-- No prayers when placing Gifts in Chalice -->
