+++
title = "Origen"
+++

# Citations by later theologians

- [Thomas Aquinas](/people/thomas-aquinas) cited Origen [over 1,000 times.](https://soundcloud.com/thomisticinstitute/thomas-aquinas-and-the-greek-fathers-for-the-renewal-of-theology-fr-andrew-hofer-op)

# Self-castration

Probably not.

- https://en.wikipedia.org/wiki/Origen#Alleged_self-castration

- [Origen himself didn't take Matthew 18:8-9 literally](https://christianity.stackexchange.com/questions/23419/did-origen-castrate-himself-under-a-literal-interpretation-of-matthew-1912)
