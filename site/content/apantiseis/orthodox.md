+++
title = "Notes on opinions and arguments from the Eastern Orthodox"
+++

# Magisterium

- [An Orthodox magisterium?](https://blogs.ancientfaith.com/nootherfoundation/an-orthodox-magisterium/) by Fr. Lawrence Farley
  [My notes](/apantiseis/orthodox/farley-magisterium)
