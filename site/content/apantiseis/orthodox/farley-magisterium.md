+++
title = "My notes on Fr. Lawrence Farley's, \"An Orthodox Magisterium?\""
+++

First, the [archive link](https://archive.ph/mRVKg).

I fisked this mechanically and with not much attempt made at polite overtures. My apologies ahead of time. I submit everything I write here to the teaching authority of the Church. I ask forgiveness anywhere I was incorrect or misleading which would be unintentional on my part in either case. Writing these things is as much a forcing-function for me to read and learn as it is an attempt to convey anything to anyone else.

The stylized blockquotes are me quoting Fr. Farley's article. Anywhere I quote something not from Fr. Farley's article, I'll be using plain quotes that look like ordinary paragraph text.

>Recently I listened to a podcast in which Larry Chapp (a universalist Roman Catholic) interviewed Dr. David Bentley Hart. In the course of the interview Dr. Hart asserted that, unlike Roman Catholicism, Orthodoxy does not have an official and authoritative Magisterium. By this he meant that Orthodoxy possesses no institutional organ (such as the papacy and the episcopate dependent upon it) that can routinely and authoritatively declare what is or is not the official teaching of the Church when consulted.

Dr. Hart's probably right about that one at the universal level but not the local level. There's no mechanism for objectively resolving differences between bishops or whole churches in the Orthodox Church.

>The word “magisterium” comes from the Latin word “magister”, meaning “teacher”.  The Magisterium of the Roman Catholic Church is therefore the organ which can and does articulate the official teaching of the Roman Church.

Sure.

>After Vatican I the Magisterium in the Roman Catholic Church effectively collapsed into the papacy, despite the later conciliar refinements of Vatican II.

This doesn't sound accurate to me. The main fight over Vatican I within the Catholic communion had to do with the [Pope's jurisdiction and munus regendi in Pastor Aeternus](https://www.ewtn.com/catholicism/teachings/vatican-is-dogmatic-constitution-pastor-aeternus-on-the-church-of-christ-243), not his munus docendi.

Papal infalliblity and the Pope's teaching authority were already infallibly taught de fide tenenda by the universal and ordinary magisterium of the Church. Vatican I advanced it to a de fide credenda proposition by an extraordinary act of the magisterium following an ecumenical council. It was already binding on the conscience.

>Bluntly put, whatever the Pope authoritatively declares ex cathedra is the official teaching of the Roman Catholic Church.

[Michael Lofton](https://reasonandtheology.com/2022/10/28/an-eastern-orthodox-magisterium-response-to-fr-lawrence-farley/) has already very ably responded to these issues in his video response but I prefer text and wanted to write out my own notes on this anyway.

I'm not sure what official means here. I'm guessing he's unknowingly painting over the variegation of the magisterium. An official teaching of the Catholic Church can come from a local bishop teaching fallibly, that would be part of the authentic or authoritative magisterium. Most of what the Pope teaches falls under this rubric as well. That doesn't mean it can or should be lightly ignored.

[Donum veritatis](https://www.vatican.va/roman_curia/congregations/cfaith/documents/rc_con_cfaith_doc_19900524_theologian-vocation_en.html) is really good on this topic. e.g.

"It follows that the sense and the weight of the Magisterium's authority are only intelligible in relation to the truth of Christian doctrine and the preaching of the true Word. The function of the Magisterium is not, then, something extrinsic to Christian truth nor is it set above the faith. It arises directly from the economy of the faith itself, inasmuch as the Magisterium is, in its service to the Word of God, an institution positively willed by Christ as a constitutive element of His Church."

Lets zoom in on "whatever the Pope authoritatively declares ex cathedra" and go to what Pastor Aeternus says:

"the Roman Pontiff, when he speaks ex Cathedra, that is, when in discharge of the office of Pastor and Teacher of all Christians, by virtue of his supreme Apostolic authority, he defines a doctrine regarding faith or morals to be held by the Universal Church, by the divine assistance promised to him in blessed Peter, is possessed of that infallibility with which the divine Redeemer willed that His Church should be endowed for defining doctrine regarding faith or morals: and that therefore such definitions of the Roman Pontiff are irreformable of themselves, and not from the consent of the Church."

>The Roman Catholic Magisterium is clear and easy to consult.

If you own a copy of Denzinger or Ott then maybe. I wouldn't take everything in Ott as gospel though. By comparison you need to be a scholar attached to an academic institution to get anywhere with Orthodox discernment of patristics and church history.

Here's a free [pre-Vatican II copy of Denzinger at archive.org](https://archive.org/details/DenzingerTheSourcesOfCatholicDogma/page/n699/mode/2up). 

>The pronouncements of popes are easy to find and quite numerous.

There are some old papal bulls that could be kinda hard to find copies of but I guess by comparison, yeah. We've lost the acta of at least one ecumenical council that I can think of. Probably more but I'm not sure and can't be bothered to check.

>Some Roman Catholics may dispute whether this or that encyclical was given ex cathedra, and therefore is or is not a part of the Church’s magisterial teaching, but the basic body of what constitutes Roman Catholic teaching is not in doubt.

Fr. Farley is more confident in the clarity and accessibility of the magisterium of the Church than I am. It's amusing to imagine someone saying this in all sincerity before going on to explain why they _aren't_ Catholic.

>The old saying Roma locuta; causa finita est (i.e. “Rome has spoken; the matter is settled”) may not be honoured as fully as in previous times, but there is no doubt of what Roma has locuted. Like it or not, Roman Catholic teaching is fairly clear. That is one of the advantages of a church organized around such a central authority. It is very efficient.

I guess. It's not a central authority because it's efficient. We have the papal office because that's what Christ instituted when he founded the Church.

Cf.
- Patristic quotes on:
  * [the authority of the Pope](https://www.churchfathers.org/authority-of-the-pope)
  * [the origins of St. Peter as Pope](https://www.churchfathers.org/origins-of-peter-as-pope)
  * [St. Peter's primacy](https://www.churchfathers.org/peters-primacy)
  * [St. Peter's successors](https://www.churchfathers.org/peters-successors)
- Ecumenical councils:
  - Second council of Lyon:
    * "we implore [the cardinals] consider very carefully what they are about to do. They are electing the vicar of Jesus Christ, the successor of Peter, the ruler of the universal church, the guide of the Lord’s flock."
    * [https://www.papalencyclicals.net/councils/ecum14.htm](https://www.papalencyclicals.net/councils/ecum14.htm)
  - Council of Florence:
    * The last attempt at re-union, this re-union was not completed in the East because Constantinople was conquered a few years after the council finished. The Ottomans supported and encouraged anti-unionist Orthodox clergy and hierarchs to mitigate the threat of a reunited Christendom.
    * "We likewise define that the holy Apostolic See, and the Roman Pontiff, hold the primacy throughout the entire world; and that the Roman Pontiff himself is the successor of blessed Peter, the chief of the Apostles, and the true vicar of Christ, and that he is the head of the entire Church, and the father and teacher of all Christians; and that full power was given to him in blessed Peter by our Lord Jesus Christ, to feed, rule, and govern the universal Church."

By the standards of the pre-1054 Eastern churches, the second council of Lyon possibly and the council of Ferrara-Florence definitely met the standard for an ecumenical council. Reception theory was developed in the Orthodox communion after the council of Florence because it created a theological problem. Even Romanides recognizes reception theory exists nowhere in the acta or canons of the ecumenical councils:

"Theologians such as Fr. John S. Romanides have argued, however, that the councils universally regarded as ecumenical within the Orthodox Church seemed of themselves to have no sense of requiring a reception by the Church before they went into effect. Their texts do indeed include self-declarations of their ecumenicity, and in most cases, their decrees immediately were written into Roman imperial law. No condition of later reception is reflected in the councils' texts." [from orthodoxwiki](https://orthodoxwiki.org/Ecumenical_Councils)

Here's [a Reason and Theology hosted debate between Ybarra and Truglia on reception theory](https://www.youtube.com/watch?v=64GvgtYRlu8).

>Dr. Hart is correct in asserting that Orthodoxy possesses no comparable organ of authoritative teaching which can declare in the same way what is de fide for the Orthodox as quickly as does the Roman Catholic Magisterium for Roman Catholics.

Nothing universal, no. If two bishops disagree on something, you don't have anywhere to go from there objectively. You could apply your own judgment, but then what is the teaching function of the Church for if it falls apart when things are _more_ difficult or uncertain such that the bishops aren't unanimous on it?

>(Nor, come to that, did the early Church—hence all those messy and sometimes contradictory councils.)

The distinction here should be made between robber councils and authentic ecumenical councils. The Eastern churches used to know how to authenticate an ecumenical council using facts on the ground at the time it happened or shortly thereafter, what changed?

"But the word of the Lord which came through the ecumenical Synod at Nicæa, abides forever." - [Athanasius, to the Bishops of Africa](https://www.newadvent.org/fathers/2819.htm), references 1 Peter 1:25

See "Papal ratification and the ecumenical councils" later in this page.

>Given Dr. Hart’s determined embrace of universalism and his (muted) appreciation of Gnosticism, it is hard not to conclude that Dr. Hart emphasizes that Orthodoxy has no Magisterium because he would find the existence of authoritative declarations of truth from the past unwelcome. The Church in the past centuries has declared against universalism and taken a much more jaundiced view of Gnosticism than that of Dr. Hart, and I submit that Dr. Hart finds this uncongenial.

I'm not interested in defending DBH's more tenuous positions. That being said, while there might've been a particular bishop who taught against universalism, it hasn't been decided in a universal and definitive way for the Orthodox because it _cannot_ be. There is no Orthodox means of doing so and no way to authenticate anything claiming to be universally binding. There's no emperor since Constantine XI Palaeologus died heroically at Fall of Constantinople and you deny the Pope's authority. Who's going to convoke or ratify a council for you, Erdoğan?

>The answer for him therefore lies in declaring that the Church’s settled view about universalism and Gnosticism do not therefore constitute an impediment to his alternate views. Again, to state it bluntly, Dr. Hart believes that since there is no authoritative Magisterium in the Orthodox Church, he is free as a good and loyal Orthodox to dissent dramatically from the Church’s historical positions.

Dissenting on the Church's historical position regarding the eternity of Hell seems unwise to me. Sententia communis at a minimum, right?

>This leads to the question: Is there an Orthodox equivalent of the Roman Catholic Magisterium? Or (put differently) does the Orthodox Church offer authoritative teaching as does the Roman Catholic Church?

Locally yes, authoritative yes, universal and definitive no.

Also what is with all the double spaces all over this blog post? I keep having to delete them as I go through this. We have modern typesetting now, you don't need to do that.

>In Orthodoxy there is no single source of authoritative teaching (such as the papacy) to which it can resort to find the final and reliable truth in matters such as the full divinity of Christ or the legitimacy of icons. But that does not mean that the Church does not teach authoritatively or offer a body of truth regarding doctrine and morals.

There isn't a single exclusive source of "authoritative teaching" in the Catholic Church either. The bishops are the successors to the apostles and not mere branch managers!

>What then constitutes authority in the Orthodox Church? The ultimate and only external authority in the Church is Christ Himself. 

This doesn't clarify anything or add to this position. Catholics hold that the ultimate authority in the Church is Christ as well. I'm not 100% positive what external means here. External to what? The body of Christ? Christ is the Christ. This has a strong whiff of Protestant rhetoric.

Thinking analogously here:

```
visible icon of the invisible body of Christ : the Church :: visible icon of the invisible head of the body : ?
```

An invisible and complete head-and-body with a visible body that has no visible head. So the Orthodox Church broadly speaking is a visible body without a head. Headless bodies are dead bodies. Unless you think the Ecumenical Patriarch is the new Pope or primate of all-Orthodox. Which would be interesting to tease out but probably inconsistent with the church fathers and councils.

>And Christ reveals Himself through His Body, the Church.

"Green is the best color because it is the greenest of all colors"

Who is the Church? What sacrament of unity and proper authority does the Church authenticate itself with?

>This means that revelation comes in many and varied forms.

"In many and various ways God spoke of old to our fathers by the prophets, but in these last days he has spoken to us by a Son." Christ, the Son of God made man, is the Father's one, perfect and unsurpassable Word. In him he has said everything; there will be no other word than this one. St. John of the Cross, among others, commented strikingly on Hebrews 1:1-2:"

"Public revelation is therefore confined to the teachings, preaching, miracles, and signs of God written down in the scriptures and passed on through sacred Tradition. Together, sacred Tradition and the sacred scriptures comprise the deposit of faith.", [from a nice summary by Cora Evans](https://www.coraevans.com/blog/article/public-revelation-what-who-when-where-how)

>The teaching and life that Christ revealed to the apostles (the so-called “good deposit”; see 2 Timothy 1:14) was spread throughout the world. It can be found in the Scriptures, and in the oral traditions of the Church (such as the tradition of facing east for prayer and of making the Sign of the Cross).

Highlighting this part:

**such as the tradition of facing east for prayer and of making the Sign of the Cross**

That's not part of the deposit of faith, but that doesn't mean they're to be handled lightly. Don't forget you've got the Old Believers excoriating Nikonites for changing how many fingers they cross themselves with. The deposit of faith cannot change, so why could that change unless it wasn't part of the deposit of faith? This is a strange thing to include in the deposit of faith. The very earliest still-extant churches don't seem to have been built in a manner designed to face East. Facing East is a venerable tradition and one I'd like to see preserved in the Church.

>It can be found in the consensus of the Fathers, since despite their diversity they share a common core of teaching which ultimately came from the apostles.

Sure.

>This core faith is expressed in the liturgical tradition (including the hymns of the Church), and in the Church’s art.

What?

>It is set forth in the decisions and work of the councils which were accepted throughout the centuries as expressions of the Church’s mind and faith (the so-called “Ecumenical Councils”, gatherings which were finally accepted as reliable by the Church throughout the ecumene or world).

Reception theory. No. See what I mentioned earlier from Romanides on the ecumenical councils and the self-understanding of the Church as regards their "reception." It was understood that an authentic ecumenical council was binding immediately.

You don't need to wait hundreds of years to find out if Nestorius is a heretic.

>In short, the authoritative teaching of the Church can be discerned in the life the Church lives throughout the centuries. The many aspects of this life—Scripture, oral tradition, liturgy, hymns, art, councils—all converge to teach the same things, and in this convergence we can discern the authoritative truth.

Discernment doesn't give you an objective magisterium. You're just sifting through sources and weighing the sources according to subjective criteria. "But the Holy Spirit!" the Cathars said they were discerning in cooperation with the Holy Spirit too and they're dead and gone.

>This is, of course, messier than the Roman Catholic model, because it involves the work of discernment. But it is also less dependent upon a single source, such as insights and decisions of a single bishop.

We're not "dependent" upon the Pope. Popes usually do not speak on an issue unless there is a need for clarity, often due to disagreement or uncertainty. Orthodox can have two bishops "discern" their way to two different conclusions.

The eternity of Hell is a teaching that very evidently commands at minimum religious assent from Catholics _and that's true even if you take all papal teaching out of the question._ `tuto doceri non potest`: it cannot be safely taught. That's assuming the ordinary magisterium of the universal church hasn't taught the eternity of Hell definitively.

>A single bishop can be wrong (remember Pope Honorius?),

[Obligatory Erick Ybarra on Pope Honorius](https://erickybarra.wordpress.com/2017/11/06/pope-honorius-the-heretic-achilles-heel-for-catholicism-and-the-papacy/)

>but it is unlikely that all the various sources of the Church will be wrong. If they all point to the same thing, they can be depended upon. One thinks of the words of G. K. Chesterton, who said that a man is not really convinced of something when he finds that something proves it. He is only really convinced when he finds that everything proves it (from his “Paradoxes of Christianity” in Orthodoxy). And the Church’s Tradition is not one thing, but many things, all of which unite to provide proof of Christ’s truth.

We have much of the same as Catholics, we just also have the universal scope and clarity of papal teaching when it is called for.

"If they all point to the same thing, they can be depended upon." ironically there's no attempt being made here to substantiate this understanding of the magisterium with patristic citations or to flesh it out with objective criteria. St. Vincent of Lerins, etc.

>This means that certain things in the Church’s history do not form a part of her Tradition because they are not in conformity with everything else.  The teaching of what has been called “the Real Presence” of Christ’s Body and Blood is part of the Tradition because it is witnessed to by Scripture, patristic consensus, liturgy, and much else.

"conformity with everything else", Lerins, etc. etc. this is a series of repetitive assertions and not the assembly of an argument for making a case.

"liturgy", there's more liturgy out there than the Byzantine rite. Which liturgies shall be consulted? Which churches' traditions? Who gets to claim that their tradition is the right one? Michael Lofton hammered in on these issues in his video that I linked earlier very well.

>Universalism is not a part of the Tradition because it was the minority report of a few whose views were set aside in favour of the majority of the Fathers, the witness of Scripture, Christian hymnography, iconography, and conciliar decrees. Paraphrasing Chesterton, the Church believes that the terrible reality of an eternal hell is part of the Tradition because everything in its long historical life proves it.

I touched on the magisterial status of Hell's eternity a bit earlier. I think this is the safe position to take. I'm not going to attempt to address Balthasarian "Dare to Hope"-ism because quite frankly I'm tired of reading about universalism. Setting aside my actual beliefs, whether I believe Hell is eternal or not shouldn't in principle actually result in different behaviors or decisions on my part. Given that and the extreme minority status of the position in the church fathers, I'd prefer to study something else.

>This, then, is the totality of the Orthodox Magisterium. The usual short form for it is “the Fathers”, a phrase to which councils and later Fathers themselves returned again and again. The idea is that where a consensus exists among the Fathers (such as regarding the Real Presence) this consensus witnesses to the prior diffusion of apostolic teaching. How else could the same teaching be found early and everywhere throughout the world among Fathers who are so different from one another?

How do you identify a consensus? How many fathers need to have spoken on the issue? How many can dissent w/ the majority still deciding the issue? Is the standard the formula of St. Vincent of Lerins? You still lack an actual objective magisterium. Appealing to discernment makes this far more Protestant than your criticisms of them would lead me to believe you are comfortable with.

As Catholics we can traffic in subjective weighing of multiple sources just as well as the Orthodox. In addition to that we have what we understand to be the fullness of what Christ established for his Church.

>This consensus finds expression in liturgy, artwork (i.e. icons), hymnography, and sometimes, the work of councils.

The liturgy is a source that Catholics will discuss from time to time as a secondary or tertiary reference but these aren't objective either.

>When an issue arises needing articulation or clarification, the immediate Protestant response is to ask, “What does the Bible say?”—a response which hides the fact that the Bible is not self-interpreting and so requires a tradition of teaching to interpret it.

He's right about the problem the Protestants have here. The bible is not an infallible self-interpreting rule of faith, there has to be an _objective_ magisterial source for clarifying questions on faith and morals. The problem is that the Orthodox do not have an _objective_ magisterium.

>The immediate Roman Catholic response is to inquire, “What has Rome said?”

This is a caricature. I'll use papal teaching when it's available for a topic but I spend far more of my time digging through patristics. The dogmatic minimalists are wrong, but the popes have decided fewer matters definitively or authoritatively than Fr. is making it sound here.

>The Orthodox respond by asking, “What do the Fathers say?” And, as it turns out, the Fathers say rather a lot, and their consensus is usually clear—sometimes and for some people, too clear. Any attempt at obfuscation in this face of this clarity (usually attempted with as many multi-syllabic words as possible) is futile and unworthy. Dr. Hart’s suggested alternative might not be a free-for-all like in Protestantism, but it is definitely like playing tennis with the net down. And, as all the Fathers would remind us, that is not how the game is played.

Agreed.

## Notes on papal ratification and the ecumenical councils

- Nicaea, 325 A.D.
  * Papal legates; possibly including Hosius or Ossius, who presided

- Constantinople, 381 A.D.
  * No pope and no legates
  * Wasn't originally intended to be an ecumenical council, there were only 150 bishops from Thrace.
  * Later ratified by Pope Gregory the Great

- Ephesus, 431 A.D.
  * Papal legates Arcadius, Projectus, and Philip

From the acts of Ephesus:

"There is no doubt, and in fact it has been known in all ages, that the holy and most blessed Peter, prince (ἔξαρχος) and head of the Apostles, pillar of the faith, and foundation (θεμέλιος) of the Catholic Church, received the keys of the kingdom from our Lord Jesus Christ, the Saviour and Redeemer of the human race, and that to him was given the power of loosing and binding sins: who down even to to-day and forever both lives and judges in his successors. The holy and most blessed pope Cœlestine, according to due order, is his successor and holds his place, and us he sent to supply his place in this holy synod, which the most humane and Christian Emperors have commanded to assemble, bearing in mind and continually watching over the Catholic faith. For they both have kept and are now keeping intact the apostolic doctrine handed down to them from their most pious and humane grandfathers and fathers of holy memory down to the present time, etc." [Concilium Ephesenum, page 338](http://www.documentacatholicaomnia.eu/03d/0431-0431,_Concilium_Ephesenum,_Documenta_Omnia_[Schaff],_EN.pdf)

- Chalcedon, 451 A.D.
  * Papal legate Paschasinus presided
  * Pope Leo I ratified the doctrinal decrees on 21 March 453, but rejected canon 28 since it ran counter to the canons of Nicaea and to the privileges of particular churches. [papalencyclicals.net](https://www.papalencyclicals.net/councils/ecum04.htm)

- Constantinople, 553 A.D.
  * No pope and no legates, due to imperial strong-arm tactics and imprisonment of Pope Vigilius
  * [Erick Ybarra on this council and collegiality](https://erickybarra.wordpress.com/2021/04/28/papacy-and-episcopacy-each-needs-each-other/)
  * Later ratified by Pope Vigilius when he was free and not under coercion

- Constantinople, 681
  * Three papal legates presiding
  * Pope Agatho's letter in the acts of the council is remarkable to say the least.

- Nicaea, 787 A.D.
  * Papal legates archpriest Peter and abbot Peter
  * Papally approved convocation
  * [For more on this, see Erick Ybarra's blog](https://erickybarra.wordpress.com/2021/11/05/response-to-objections-concerning-the-papal-claims-of-pope-hadrian-i-at-the-2nd-council-of-nicaea-787/)

## Notes on the magisterium

There's been some confusion over the magisterium I think in part because people are taking a multi-dimensional issue and trying to flatten it down to a 1d list of categories. Here's my attempt:

Magisterial context or mode:
- Extraordinary, all universal
  * Ecumenical council
  * Apostolic constitution or encyclical from a Pope
- Ordinary
  * Pope teaching ordinarily
  * Bishops teaching ordinarily
  * Bishops, together with the Pope, dispersed but in agreement. Could be infallible if proposed definitively by all.

What does a magisterial teaching demand of us?

- de fide credenda
  * ["teachings defined as explicitly and specifically revealed in the deposit of faith"](https://en.wikipedia.org/wiki/Infallibility_of_the_Church#Catholic_Church)
- de fide tenenda
  * "equally infallible but are proposed not as being explicitly in the deposit of faith, but nevertheless implied by it or intrinsically connected to it logically or historically"
- obsequium religiosum (religious assent, fallible, Lumen gentium 25: see below)

Infallibility:
- Infallible
  * Papal infallibility was already de fide tenenda in the universal magisterium of the Church and thus infallibly taught and absolutely binding on the conscience of all Catholic Christians. Vatican I advanced it to de fide credenda by an act of the extraordinary magisterium of the Pope following an ecumenical council in consultation with all the bishops in communion with the earthly head of the church.
  * The Church as a whole, through the ordinary-and-universal magisterium, teaches infallibly.
    - When: a teaching concerning a matter of faith and morals that all the bishops of the Church (including the Pope) hold as definitive
    - The assent of all of the bishops was explicitly sought out in the run up to [Evangelium vitae](https://en.wikipedia.org/wiki/Evangelium_vitae#Authority).
- Fallible
  * Authoritative/authentic magisterium, this is most of the magisterium by sheer volume. This is exercised by every bishop in the world within their own diocese, not just the Pope.

### Re: religious assent, here's the relevant part of Lumen Gentium 25:

"Bishops, teaching in communion with the Roman Pontiff, are to be respected by all as witnesses to divine and Catholic truth. In matters of faith and morals, the bishops speak in the name of Christ and the faithful are to accept their teaching and adhere to it with a religious assent. This religious submission of mind and will must be shown in a special way to the authentic magisterium of the Roman Pontiff, even when he is not speaking ex cathedra; that is, it must be shown in such a way that his supreme magisterium is acknowledged with reverence, the judgments made by him are sincerely adhered to, according to his manifest mind and will. His mind and will in the matter may be known either from the character of the documents, from his frequent repetition of the same doctrine, or from his manner of speaking."

### Spirago on the collegiality and the consultative process that informs papal teaching

"When the Pope gives a decision on a doctrinal matter, it is Christ Who keeps him from error by the agency of the Holy Ghost; moreover the bishops are always consulted before any such decision is given… Although the Pope is infallible in his solemn decisions, general councils are not for that reason superfluous; for the confer a greater external solmenity on the Pope’s decrees, and the teaching of the Church can be more thoroughly examined in these assemblies. Hence these general councils may, under certain circumstances, be necessary as well as useful. Even the apostles held a general council at Jerusalem, though each single apostle was infallible in his office as teacher." Rev. Fr. Francis Spirago, The Catechism Explained
