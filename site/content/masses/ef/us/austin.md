+++
title = "Extraordinary form Masses (Diocese of Austin)"
paginate_by = 5
+++

## Diocesan

### Austin

#### St. Mary's Cathedral

**Address:**
203 East 10th Street
Austin, TX 78701

**Phone:**
[(512) 331-1343](tel:+15123311343)

**Clergy:**
Father Daniel Liu

**Mass Times:**
- Sunday
  * 3:30 PM

Holy days of obligation according to the old calendar are observed with an EF Mass. First Fridays of the month have confession at 8:30 AM and EF Mass at 9:30 AM. Mass is followed by adoration.

#### Austin Airport Marion

**Address:**
S. 4415 S. I-35 Hwy
Austin, TX

**Mass Times:**
- 2nd & 4th Sunday 4:00 pm

Note: SSPX.

### Brenham

#### St Mary of the Immaculate

**Address:**
701 Church Street
Brenham, TX

**Phone:**
[(979) 656-4441](tel:+19796564441)

**Clergy:**
Fr Bradford Hernandez

**Mass Times:**
- Sunday
  * 4:00 PM
