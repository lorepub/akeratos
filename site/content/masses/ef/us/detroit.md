+++
title = "Extraordinary form Masses (Archdiocese of Detroit)"
paginate_by = 5
+++

## St Joseph Oratory (Detroit)

ICKSP

**Mass Times:**

  - Sunday
    * 9 AM (Low Mass)
    * 11 AM (High Mass)
  - Weekdays except Wednesday: 8 AM (Low Mass)
  - Wednesday: 12 PM (Low Mass)
  - Saturday: 9 AM (Low Mass)

**Address:**
1828 Jay St

**Phone:**
[(313) 784-9152](tel:+13137849152)

**[Website](http://institute-christ-king.org/detroit/)**

**Clergy:**

 - Canon Michael Stein (Rector)
 - Canon Adrian Sequeira (Vicar)

## Assumption Grotto (Detroit)

Diocese

**Mass Times:**

  - Sunday: 9:30 AM
  - Weekdays and Saturday: 7:30 AM

**Address:**
13770 Gratiot Avenue

**Phone:**
[(313) 372-0762](tel:+13133720762)

**[Website](https://assumptiongrotto.wordpress.com/)**

**Clergy:**
  - Fr. Eduard Perrone (Pastor)
  - Fr. John Bustamante (Associate Pastor)

## Old St. Mary's (Detroit)

Diocese

**Mass Times:**

  - Sunday: 10 AM

**Address:**
646 Monroe Ave.

**Phone:**
[(313) 961-8711](tel:+13139618711)

**[Website](http://oldstmarysdetroit.com/)**


## St Edward-on-the-Lake (Lakeport)

Diocese

**Mass Times:**

 - Sunday: 8 AM
 - Saturday (1st) 8 AM

**Address:**
6945 Lakeshore Rd

**Phone:**
[(810) 385-4340](tel:+18103854340)

**[Website](https://stedwardonthelake.org/)**

**Pastor:**
Fr. Lee Acervo

