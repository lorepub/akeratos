+++
title = "Perpetual virginity of Mary"
+++

## Reasons to believe Mary was virgin at the time of Christ's birth

Recapping some points from [question 28 of Thomas Aquinas' Summa Theologiae](http://www.newadvent.org/summa/4028.htm):

- [Isaiah 7:14](https://www.biblegateway.com/passage/?search=Isaiah+7%3A14&version=RSVCE), _'almah_ is translated in the [Septuagint](/documents/septuagint) as παρθένος meaning virgin.

- [Matthew 12:49-50](https://www.biblegateway.com/passage/?search=Matthew+12%3A49-50&version=RSVCE), here _brethren_ isn't "brothers and sisters" as in siblings. Aramaic didn't have a word for cousin like Greek did. The [Septuagint](/documents/septuagint) has a similar pattern where they don't use the word for cousin and instead say brethren.

## [The Protoevangelium of James](/documents/protoevangelium-of-james) and other apocrypha

> And [from the time she was three] Mary was in the temple of the Lord as if she were a dove that dwelt there

> 'You have been chosen by lot to take into your keeping the Virgin of the Lord.' But Joseph refused, saying, 'I have children, and I am an old man, and she is a young girl'

> He has defiled the virgin whom he received out of the temple of the Lord and has married her by stealth

> As the Lord my God lives, I am pure before him, and know not man

Apart from the protogospel of James, other apocrypha assert the perpetual virginity of Mary including:

- [The Acts of Peter](http://www.earlychristianwritings.com/actspeter.html), [Wikipedia](https://en.wikipedia.org/wiki/Acts_of_Peter)

<!-- - Book of Sybils 
I can't track down what this is and it seems iffy.
-->

- [The Ascension of Isaiah](http://www.earlychristianwritings.com/ascension.html) "And he did not approach Mary, but kept her as a holy virgin, though with child." <br> This source also affirms that [Mary didn't experience labor pains](/mary/birth-pain), a commonplace in Church Tradition.

<!-- For these points, please see this article from [from the University of Dayton](https://udayton.edu/imri/mary/p/perpetual-virginity-in-the-early-church.php).
Not sure about this source.
-->

We can take apocryphal sources for what they are, but this hints at how widespread the belief was in the early Church.

## Church Fathers

> [Clement of Alexandria](/people/clement-of-alexandria) reportedly [accepted the Protoevangelium of James without objection](https://udayton.edu/imri/mary/p/perpetual-virginity-in-the-early-church.php).

> [Origen](/people/origen)

> Therefore let those who deny that the Son is from the Father by nature and proper to His Essence, deny also that He took true human flesh of Mary Ever-Virgin; for in neither case had it been of profit to us men, whether the Word were not true and naturally Son of God, or the flesh not true which He assumed. <br> [Athanasius](/people/athanasius). _[Discourses Against the Arians, 2:70](http://www.newadvent.org/fathers/28162.htm)_. 360 A.D.

## Further resources

- [churchfathers.org](https://www.churchfathers.org/mary-ever-virgin/)
