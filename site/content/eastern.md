+++
title = "Information about Eastern Christianity"

[extra]
notitle = true
+++

I kept the label for this category general (Eastern Christian) because you can't really dig into Eastern Catholicism without leaning on or at least examining Eastern Orthodox sources.

# Rites of the Eastern churches

[Rites and recensions of the Eastern churches](/eastern/rites)
