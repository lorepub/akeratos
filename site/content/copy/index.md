+++
title = "copyright/license info"

[extra]
notitle = true
+++

&copy; 2018-2019 Chris Allen

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">
    <img alt="Creative Commons License" style="border-width:0" src="cc-by-nc-nd.png" />
</a>

The code and infrastructure, found in the [akeratos.com gitlab
repo](http://gitlab.com/akeratos/akeratos.com/) is licensed according to the
details found in the LICENSE file. The original repository was: [https://gitlab.com/raptros/raptros.com](https://gitlab.com/raptros/raptros.com)
